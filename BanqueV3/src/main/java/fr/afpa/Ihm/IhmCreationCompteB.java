package fr.afpa.Ihm;

import javax.swing.JFrame;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import java.awt.Dimension;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;

import fr.afpa.Dto.DtoComptes;
import fr.afpa.Entite.Client;
import fr.afpa.Entite.CompteBancaires;

import java.awt.Color;
import javax.swing.border.LineBorder;

public class IhmCreationCompteB extends JFrame implements ActionListener {

	private JPanel contentPanePrincipal;
	private JTextField enreeNumCompte;
	private JTextField entreeSolde;
	private JTextField txtCodeAgence;
	private boolean actif;
	private boolean autorise;
	private JComboBox choixTypeCompte;
	private JCheckBox chckbxCompteActif;
	private JCheckBox chckbxDecouvertAutoris;
	private Client client;

	public IhmCreationCompteB(Client clientTrouve) {
		client = clientTrouve;

		setTitle("Banque CDA-V3");
		setResizable(false);
		setMinimumSize(new Dimension(1200, 800));
		setFont(new Font("Cambria", Font.PLAIN, 18));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnUtilisateur = new JMenu("Utilisateur");
		menuBar.add(mnUtilisateur);

		JMenuItem mntmDeconnexion = new JMenuItem("Deconnexion");
		mnUtilisateur.add(mntmDeconnexion);

		JMenuItem mntmAcceuil = new JMenuItem("Acceuil");
		mnUtilisateur.add(mntmAcceuil);

		JMenu mnAide = new JMenu("Aide");
		menuBar.add(mnAide);
		contentPanePrincipal = new JPanel();
		contentPanePrincipal.setBackground(Color.WHITE);
		contentPanePrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanePrincipal);
		contentPanePrincipal.setLayout(null);

		JPanel panelAvecBordure = new JPanel();
		panelAvecBordure.setFont(new Font("Cambria", Font.PLAIN, 18));
		panelAvecBordure.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229), 2, true),
				"Creation Nouveau Compte Bancaire", TitledBorder.LEFT, TitledBorder.TOP, null, new Color(51, 51, 51)));
		panelAvecBordure.setBounds(207, 231, 779, 281);
		contentPanePrincipal.add(panelAvecBordure);
		panelAvecBordure.setLayout(null);

		JLabel lblNumeroDuCompte = new JLabel("Numero du Compte Bancaire:");
		lblNumeroDuCompte.setBounds(12, 40, 300, 24);
		panelAvecBordure.add(lblNumeroDuCompte);
		lblNumeroDuCompte.setFont(new Font("Cambria", Font.PLAIN, 22));
		lblNumeroDuCompte.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNumeroDuCompte.setLabelFor(enreeNumCompte);

		enreeNumCompte = new JTextField();
		enreeNumCompte.setBounds(345, 36, 426, 28);
		panelAvecBordure.add(enreeNumCompte);
		enreeNumCompte.setColumns(10);

		JLabel lblSolde = new JLabel("Solde:");
		lblSolde.setBounds(140, 74, 172, 16);
		panelAvecBordure.add(lblSolde);
		lblSolde.setFont(new Font("Cambria", Font.PLAIN, 22));
		lblSolde.setHorizontalAlignment(SwingConstants.RIGHT);

		entreeSolde = new JTextField();
		entreeSolde.setBounds(345, 71, 426, 28);
		panelAvecBordure.add(entreeSolde);
		entreeSolde.setColumns(10);

		JLabel lblTypeDeCompte = new JLabel("Type de Compte :");
		lblTypeDeCompte.setBounds(129, 102, 183, 27);
		panelAvecBordure.add(lblTypeDeCompte);
		lblTypeDeCompte.setFont(new Font("Cambria", Font.PLAIN, 22));
		lblTypeDeCompte.setHorizontalAlignment(SwingConstants.RIGHT);

		// creation de la JCombox : selection du choix du type de compte
		choixTypeCompte = new JComboBox();
		choixTypeCompte.setBounds(345, 111, 426, 27);
		panelAvecBordure.add(choixTypeCompte);
		choixTypeCompte.setFont(new Font("Cambria", Font.BOLD, 18));
		choixTypeCompte.setModel(new DefaultComboBoxModel(new String[] { "Compte courant", "Livret A", "Plan Epargne" }));

		// checkbox pour activer le compte
		chckbxCompteActif = new JCheckBox("Compte Actif");
		chckbxCompteActif.setBounds(345, 153, 253, 24);
		
		panelAvecBordure.add(chckbxCompteActif);
		chckbxCompteActif.setFont(new Font("Cambria", Font.PLAIN, 17));

		// checkbox pour savoir decouvert est autorise
		chckbxDecouvertAutoris = new JCheckBox("Decouvert Autorisé");
		chckbxDecouvertAutoris.setBounds(345, 183, 199, 33);
		
		panelAvecBordure.add(chckbxDecouvertAutoris);
		chckbxDecouvertAutoris.setFont(new Font("Cambria", Font.PLAIN, 17));

		// bouton pour valider toutes les informations recoltés .
		JButton btnValider = new JButton("Valider");
		btnValider.setBounds(345, 224, 124, 26);
		btnValider.addActionListener(this);
		panelAvecBordure.add(btnValider);

		// bouton pour annuler toute la creation et retour acceuil
		JButton btnAnnuler = new JButton("Annuler Tout");
		btnAnnuler.setBounds(480, 224, 124, 26);
		btnAnnuler.addActionListener(this);
		panelAvecBordure.add(btnAnnuler);

		JLabel lblAgence = new JLabel("Code Agence :");
		lblAgence.setFont(new Font("Cambria", Font.PLAIN, 18));
		lblAgence.setBounds(92, 154, 108, 22);
		panelAvecBordure.add(lblAgence);

		txtCodeAgence = new JTextField();
		txtCodeAgence.setFont(new Font("Dialog", Font.PLAIN, 14));
		txtCodeAgence.setBounds(212, 153, 61, 24);
		panelAvecBordure.add(txtCodeAgence);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if("Annuler Tout".equals(e.getActionCommand())) {
			this.dispose();
			new IhmEcranAccueil();
			
		}
		if("Valider".equals(e.getActionCommand())) {
			DtoComptes dtoComp=new DtoComptes();
					
			CompteBancaires compte=new CompteBancaires();
			compte.setActif(chckbxCompteActif.isSelected());
			compte.setDecouvert(chckbxDecouvertAutoris.isSelected());
			compte.setIdTypeCompte(choixTypeCompte.getSelectedIndex()+1);
			
			int numCompte=0;
			double solde=0d;
			try {
			numCompte=Integer.parseInt(enreeNumCompte.getText());
			solde=Double.parseDouble(entreeSolde.getText());
			compte.setIdCompte(numCompte);
			compte.setSolde(solde);
			if(dtoComp.CreationComptes(client, txtCodeAgence.getText(), compte)) {
				this.dispose();
				new IhmEcranAccueil();
			IhmMessagePersonnalisable message=	new IhmMessagePersonnalisable("Compte bancaire crée");
			message.getLblTitreErreur().setText("Compte crée");
			}
			else {
				new IhmMessagePersonnalisable("Ce code agence n'existe pas");
			}
			
			
			}catch (NumberFormatException exc) {
				new IhmMessagePersonnalisable("Pas de lettres dans les données sensibles");
				
				
			}
		
			
		}
		
		
		
	}

}
