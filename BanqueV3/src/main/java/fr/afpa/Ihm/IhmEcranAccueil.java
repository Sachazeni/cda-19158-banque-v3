package fr.afpa.Ihm;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

public class IhmEcranAccueil extends JFrame implements ActionListener{
	
	public boolean rendreVisible;

	public IhmEcranAccueil() {
			rendreVisible=true;
		
			
			JMenuBar menuBar = new JMenuBar();
			setJMenuBar(menuBar);
			
			JMenu mnUtilisateur = new JMenu("Utilisateur");
			menuBar.add(mnUtilisateur);
			
			JMenuItem mntmAcceuil = new JMenuItem("Acceuil");
			mnUtilisateur.add(mntmAcceuil);
			
			JMenuItem mntmDeconnexion = new JMenuItem("Deconnexion");
			mnUtilisateur.add(mntmDeconnexion);
			
			JMenu mnAide = new JMenu("Aide");
			menuBar.add(mnAide);
			
			
			//creation Panel princpal
			JPanel contentPanePrincipal = new JPanel();
			contentPanePrincipal.setBackground(Color.WHITE);
			contentPanePrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPanePrincipal);
			contentPanePrincipal.setLayout(null);
			
			//creation du panel central avec un titre et bordure qui va contenir les boutons
			JPanel panel = new JPanel();
			panel.setBackground(Color.WHITE);
			panel.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Bonjour, veuillez choisir une action :", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
			panel.setBounds(424, 201, 346, 346);
			contentPanePrincipal.add(panel);
			panel.setLayout(null);
			
			//boutons !!!! pas oubler de les declarer en variables!!!
			//TODO
			JButton btnNewButton = new JButton("Creation Nouveau Client");
			btnNewButton.setBounds(82, 36, 187, 26);
			panel.add(btnNewButton);
			btnNewButton.setFont(new Font("Cambria", Font.BOLD, 13));
			btnNewButton.addActionListener(this);
			
			JButton btnGestionClientExistant = new JButton("Gestion Client Existant");
			btnGestionClientExistant.setBounds(82, 98, 187, 26);
			panel.add(btnGestionClientExistant);
			btnGestionClientExistant.setFont(new Font("Cambria", Font.BOLD, 13));
			btnGestionClientExistant.addActionListener(this);
			
			JButton btnCreationNouvelleAgence = new JButton("Creation Nouvelle Agence");
			btnCreationNouvelleAgence.setBounds(82, 160, 187, 26);
			panel.add(btnCreationNouvelleAgence);
			btnCreationNouvelleAgence.setFont(new Font("Cambria", Font.BOLD, 13));
			btnCreationNouvelleAgence.addActionListener(this);
			
			JButton btnCreationConseil = new JButton("Creer Conseiller");
			btnCreationConseil.setBounds(82, 222, 187, 26);
			panel.add(btnCreationConseil);
			btnCreationConseil.setFont(new Font("Cambria", Font.BOLD, 13));
			btnCreationConseil.addActionListener(this);
			
			JButton button = new JButton("Se Deconnecter");
			button.setFont(new Font("Cambria", Font.BOLD, 13));
			button.setBounds(82, 284, 187, 26);
			panel.add(button);
			button.addActionListener(this);
			
			setTitle("Banque CDA-V3");
			setResizable(false);
			setMinimumSize(new Dimension(1200, 800));
			setFont(new Font("Cambria", Font.PLAIN, 18));
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setBounds(100, 100, 450, 300);
			setLocationRelativeTo(null);
			setVisible(rendreVisible);
		
}

	@Override
	public void actionPerformed(ActionEvent e) {
		if("Creation Nouveau Client".equals(e.getActionCommand())) {
			this.dispose();
			new IhmCreationNouveauClient();
		}else if("Gestion Client Existant".equals(e.getActionCommand())) {
			this.dispose();
			new IhmRechercheClient();
		}else if("Creation Nouvelle Agence".equals(e.getActionCommand())) {
			this.dispose();
			new IhmCreationAgence();	
		}else if("Creer Conseiller".equals(e.getActionCommand())) {
			this.dispose();
			new IhmConseiller();
		}else if("Se Deconnecter".equals(e.getActionCommand())){
			this.dispose();
			new IhmAuthentification();
		}
}
}