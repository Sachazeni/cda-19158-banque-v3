package fr.afpa.Ihm;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.text.Format;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import fr.afpa.BddServices.BddAgence;
import fr.afpa.BddServices.BddConnexion;
import fr.afpa.Dto.DtoAgence;
import fr.afpa.Entite.Agence;
import fr.afpa.Services.AgenceServices;

public class IhmCreationAgence extends JFrame implements ActionListener {
	private JTextField txtNom;
	private JTextField txtCodepostal;
	private JFormattedTextField frmTxtCodeAg;
	private JButton btnValider;
	private JButton btnAnnuler;
	private boolean rendreVisible;

	public IhmCreationAgence() {
		rendreVisible = true;

		// creation du Menu
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnUtilisateur = new JMenu("Utilisateur");
		menuBar.add(mnUtilisateur);

		JMenuItem mntmDeconnexion = new JMenuItem("Deconnexion");
		mnUtilisateur.add(mntmDeconnexion);

		JMenu mnAide = new JMenu("Aide");
		menuBar.add(mnAide);

		// creation du panel principal
		JPanel contentPanePrincipal = new JPanel();
		contentPanePrincipal.setBackground(Color.WHITE);
		contentPanePrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanePrincipal);
		contentPanePrincipal.setLayout(null);

		// creation du panel qui sera situé au milieu avec la bordure
		JPanel panelAvecBordure = new JPanel();
		panelAvecBordure.setFont(new Font("Cambria", Font.PLAIN, 18));
		panelAvecBordure.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229), 2, true),
				"Creation Nouvelle Agence", TitledBorder.LEFT, TitledBorder.TOP, null, new Color(51, 51, 51)));
		panelAvecBordure.setBounds(83, 136, 1028, 478);
		contentPanePrincipal.add(panelAvecBordure);
		panelAvecBordure.setLayout(null);

		// Creation des label pour plus de clarete cote utilisateur(quel textField
		// appartient à qui)
		JLabel lblNom = new JLabel("Nom :");
		lblNom.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNom.setFont(new Font("Cambria", Font.PLAIN, 18));
		lblNom.setBounds(190, 107, 46, 14);
		panelAvecBordure.add(lblNom);

		JLabel lblMail = new JLabel("Adresse (code postal) :");
		lblMail.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMail.setLocation(11, 352);
		lblMail.setSize(225, 17);
		lblMail.setFont(new Font("Cambria", Font.PLAIN, 18));
		panelAvecBordure.add(lblMail);

		JLabel lblCodeAg = new JLabel("Code de la nouvelle agence :");
		lblCodeAg.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCodeAg.setLocation(11, 228);
		lblCodeAg.setSize(225, 17);
		lblCodeAg.setFont(new Font("Cambria", Font.PLAIN, 18));
		panelAvecBordure.add(lblCodeAg);

		// init des zones de texte qu'il faut recuperer pour pouvoir créer une nouvelle
		// agence
		txtNom = new JTextField();
		txtNom.setBounds(254, 106, 531, 21);
		panelAvecBordure.add(txtNom);
		txtNom.setColumns(10);

		txtCodepostal = new JTextField();
		txtCodepostal.setColumns(10);
		txtCodepostal.setBounds(255, 351, 531, 21);
		panelAvecBordure.add(txtCodepostal);

		frmTxtCodeAg = new JFormattedTextField((Format) null);
		frmTxtCodeAg.setBounds(255, 227, 529, 21);
		panelAvecBordure.add(frmTxtCodeAg);

		// init du bouton valider
		btnValider = new JButton("Valider la Creation de la nouvelle Agence");
		btnValider.setFont(new Font("Cambria", Font.PLAIN, 14));
		btnValider.setBounds(504, 410, 281, 28);
		panelAvecBordure.add(btnValider);
		btnValider.addActionListener(this);

		btnAnnuler = new JButton("Annuler et Retourner à l'Acceuil");
		btnAnnuler.setFont(new Font("Cambria", Font.PLAIN, 14));
		btnAnnuler.setBounds(234, 410, 232, 28);
		panelAvecBordure.add(btnAnnuler);
		btnAnnuler.addActionListener(this);

		setTitle("Banque CDA-V3");
		setResizable(false);
		setMinimumSize(new Dimension(1200, 800));
		setFont(new Font("Cambria", Font.PLAIN, 18));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setVisible(rendreVisible);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if ("Annuler et Retourner à l'Acceuil".equals(e.getActionCommand())) {
			this.dispose();
			new IhmEcranAccueil();
		}
		if ("Valider la Creation de la nouvelle Agence".equals(e.getActionCommand())) {
			DtoAgence dtoAg = new DtoAgence();

			// ici faire une condition si le dto renvoit true faut ouvrir fenetre succes
			// sinon une fenetre false!
			if (dtoAg.initAgence(this.frmTxtCodeAg.getText(), this.txtNom.getText(), this.txtCodepostal.getText())) {
				new IhmMessagePersonnalisable("L'agence a été crée avec succé.");
			}

		}
	}
}