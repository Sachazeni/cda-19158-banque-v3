package fr.afpa.Ihm;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

public class IhmOperationVirement extends JFrame implements ActionListener {
	private JTextField txtMontant;
	private JTextField txtCompteBenef;
	private JButton btnAnnuler ;
	private JButton btnValider;
	private boolean rendreVisible;
	
	
	public IhmOperationVirement() {
		rendreVisible=true;
		
		//creation Menu
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnUtilisateur = new JMenu("Utilisateur");
		menuBar.add(mnUtilisateur);
		
		JMenuItem mntmDeconnexion = new JMenuItem("Deconnexion");
		mnUtilisateur.add(mntmDeconnexion);
		
		JMenu mnAide = new JMenu("Aide");
		menuBar.add(mnAide);
		
		
		//creation Panel princpal
		JPanel contentPanePrincipal = new JPanel();
		contentPanePrincipal.setBackground(UIManager.getColor("Button.light"));
		contentPanePrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanePrincipal);
		contentPanePrincipal.setLayout(null);
		
		//panelcentral qui va contenir le label et la zone de texte
		JPanel panelCentre = new JPanel();
		panelCentre.setBackground(Color.WHITE);
		panelCentre.setBounds(29, 88, 386, 69);
		contentPanePrincipal.add(panelCentre);
		panelCentre.setLayout(null);
		
	
		
		JLabel lblEntrezLeMontant = new JLabel("Entrez le montant :");
		lblEntrezLeMontant.setBounds(14, 3, 105, 16);
		panelCentre.add(lblEntrezLeMontant);
		
		JLabel lblCompteBnficiaire = new JLabel("Compte bénéficiaire :");
		lblCompteBnficiaire.setBounds(0, 34, 120, 16);
		panelCentre.add(lblCompteBnficiaire);
		
		//init des zones de texte
		txtMontant = new JTextField();
		txtMontant.setBounds(133, 0, 237, 20);
		panelCentre.add(txtMontant);
		txtMontant.addActionListener(this);
		
		txtCompteBenef = new JTextField();
		txtCompteBenef.setBounds(133, 32, 237, 20);
		panelCentre.add(txtCompteBenef);
		txtCompteBenef.addActionListener(this);
		
		//panel pour contenir les boutons et les centrer
		JPanel panelBoutons = new JPanel();
		panelBoutons.setBackground(Color.WHITE);
		panelBoutons.setBounds(119, 190, 205, 23);
		contentPanePrincipal.add(panelBoutons);
		panelBoutons.setLayout(null);
	
		btnAnnuler = new JButton("Annuler");
		btnAnnuler.setBounds(0, 0, 89, 23);
		panelBoutons.add(btnAnnuler);
		btnAnnuler.addActionListener(this);
		
		btnValider = new JButton("Valider");
		btnValider.setBounds(116, 0, 89, 23);
		panelBoutons.add(btnValider);
		btnValider.addActionListener(this);
		
		//le label à modiifer en fonction de l'operation demandé
	
		 JLabel lblTitreOperation= new JLabel("Virement");
		lblTitreOperation.setFont(new Font("Cambria", Font.PLAIN, 18));
		lblTitreOperation.setBounds(186, 33, 72, 22);
		contentPanePrincipal.add(lblTitreOperation);
		
		setTitle("Banque CDA-V3");
		setResizable(false);
		setMinimumSize(new Dimension(400, 150));
		setFont(new Font("Cambria", Font.PLAIN, 18));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setVisible(rendreVisible);
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		
//		if("Valider".equals(e.getActionCommand())){
//			this.dispose();
//			new IhmCompteSpecifique();
//		}
		if("Annuler".equals(e.getActionCommand())){
			this.dispose();
			//new IhmCompteSpecifique();
		}
		
	}
}
