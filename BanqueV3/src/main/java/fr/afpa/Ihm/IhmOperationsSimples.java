package fr.afpa.Ihm;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

public class IhmOperationsSimples extends JFrame implements ActionListener{
	
	
	private JTextField textField;
	private JLabel lblTitreOperation; 
	private JButton btnAnnuler;
	private JButton btnValider;
	
	public IhmOperationsSimples() {
		
		//creation Menu
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnUtilisateur = new JMenu("Utilisateur");
		menuBar.add(mnUtilisateur);
		
		JMenuItem mntmDeconnexion = new JMenuItem("Deconnexion");
		mnUtilisateur.add(mntmDeconnexion);
		
		JMenu mnAide = new JMenu("Aide");
		menuBar.add(mnAide);
		
		
		//creation Panel princpal
		JPanel contentPanePrincipal = new JPanel();
		contentPanePrincipal.setBackground(UIManager.getColor("Button.light"));
		contentPanePrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanePrincipal);
		contentPanePrincipal.setLayout(null);
		
		//panelcentral qui va contenir le label et la zone de texte
		JPanel panelCentre = new JPanel();
		panelCentre.setBackground(Color.WHITE);
		panelCentre.setBounds(47, 112, 349, 20);
		contentPanePrincipal.add(panelCentre);
		panelCentre.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(112, 0, 237, 20);
		panelCentre.add(textField);
		textField.setColumns(10);
		textField.addActionListener(this);
		
		JLabel lblEntrezLeMontant = new JLabel("Entrez le montant :");
		lblEntrezLeMontant.setBounds(0, 3, 105, 16);
		panelCentre.add(lblEntrezLeMontant);
				
		//panel pour contenir les boutons 
		JPanel panelBoutons = new JPanel();
		panelBoutons.setBackground(Color.WHITE);
		panelBoutons.setBounds(119, 177, 205, 23);
		contentPanePrincipal.add(panelBoutons);
		panelBoutons.setLayout(null);
		
	
		btnAnnuler = new JButton("Annuler");
		btnAnnuler.setBounds(0, 0, 89, 23);
		panelBoutons.add(btnAnnuler);
		btnAnnuler.addActionListener(this);
		
		btnValider = new JButton("Valider");
		btnValider.setBounds(116, 0, 89, 23);
		panelBoutons.add(btnValider);
		btnValider.addActionListener(this);
		
		//le label à modiifer en fonction de l'operation demandé
	
		lblTitreOperation= new JLabel("Intitulé de l'operation");
		lblTitreOperation.setFont(new Font("Cambria", Font.PLAIN, 18));
		lblTitreOperation.setBounds(137, 45, 169, 22);
		contentPanePrincipal.add(lblTitreOperation);
		
		setTitle("Banque CDA-V3");
		setResizable(false);
		setMinimumSize(new Dimension(400, 150));
		setFont(new Font("Cambria", Font.PLAIN, 18));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if("Annuler".equals(e.getActionCommand())) {
			this.dispose();
		//	new IhmCompteSpecifique(client);
		}
//		if("Valider".equals(e.getActionCommand())) {
//			this.dispose();
//			new IhmCompteSpecifique();
//		}
		
	}

}
