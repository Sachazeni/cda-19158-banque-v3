package fr.afpa.Ihm;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import fr.afpa.BddServices.BDDClient;
import fr.afpa.BddServices.BddConnexion;
import fr.afpa.Dto.DtoClient;
import fr.afpa.Entite.Client;

public class IhmRechercheClient extends JFrame implements ActionListener {

	private JTextField txtSaisie;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private boolean rendreVisible = true;

	public IhmRechercheClient() {
		// creation barre de menue
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		// Creation onglet utilisateur
		JMenu mnUtilisateur = new JMenu("Utilisateur");
		menuBar.add(mnUtilisateur);

		// Creation onglet aide
		JMenuItem mntmAcceuil = new JMenuItem("Acceuil");
		mnUtilisateur.add(mntmAcceuil);
		// ajout items dans l'onglet Utilisateur :
		JMenuItem mntmDeconnexion = new JMenuItem("Deconnexion");
		mnUtilisateur.add(mntmDeconnexion);
		JMenu mnAide = new JMenu("Aide");
		menuBar.add(mnAide);

		// creation Panel princpal
		JPanel contentPanePrincipal = new JPanel();
		contentPanePrincipal.setBackground(Color.WHITE);
		contentPanePrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanePrincipal);
		contentPanePrincipal.setLayout(null);

		// creation pannel qui va contenir les elements pour la recherche
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)),
				"Veuillez entrer l'identifiant ou le nom du client recherché", TitledBorder.LEADING, TitledBorder.TOP,
				null, new Color(51, 51, 51)));
		panel.setBounds(10, 11, 1174, 173);
		contentPanePrincipal.add(panel);
		panel.setLayout(null);

		// la recherche sera saisie ici
		txtSaisie = new JTextField();
		txtSaisie.setBounds(23, 83, 333, 32);
		panel.add(txtSaisie);
		txtSaisie.setColumns(10);

		// label avec le label juste pour le rappel
		JLabel lblRappel = new JLabel("Rappel : l'identifiant est composé de 2 lettres maj et de 6 chiffres");
		lblRappel.setFont(new Font("Tahoma", Font.ITALIC, 11));
		lblRappel.setBounds(23, 128, 305, 14);
		panel.add(lblRappel);

		// bouton pour lancer la recherche
		JButton btnNewButton = new JButton("Rechercher");
		btnNewButton.setBounds(366, 83, 101, 31);
		panel.add(btnNewButton);
		btnNewButton.addActionListener(this);

		// boutton pour annuler et revenir vers l'ecran d'acceuil
		JButton btnAnnulerEtRevenir = new JButton("Annuler");
		btnAnnulerEtRevenir.setBounds(477, 83, 78, 29);
		panel.add(btnAnnulerEtRevenir);
		btnAnnulerEtRevenir.addActionListener(this);

		// -------- Création des radio-boutons pour cibler si l'utilisateur souhaite
		// faire la recherche par nom ou par identifiant
		// et ajout vers le groupe des boutons pour qu'il n'y a en qu'un qui soit
		// selectionnée
		JRadioButton rdbtnChoixNom = new JRadioButton("Nom");
		buttonGroup.add(rdbtnChoixNom);
		rdbtnChoixNom.setFont(new Font("Cambria", Font.BOLD, 12));
		rdbtnChoixNom.setBounds(23, 33, 121, 24);
		rdbtnChoixNom.setActionCommand("nom");
		panel.add(rdbtnChoixNom);
		// deuxieme radio bouton
		JRadioButton rdbtnChoixIdentifiant = new JRadioButton("Identifiant");

		rdbtnChoixIdentifiant.setFont(new Font("Cambria", Font.BOLD, 12));
		rdbtnChoixIdentifiant.setBounds(165, 33, 121, 24);
		rdbtnChoixIdentifiant.setActionCommand("identifiant");
		panel.add(rdbtnChoixIdentifiant);
		buttonGroup.add(rdbtnChoixIdentifiant);
		pack();

		// init fenetre
		setTitle("Banque CDA-V3");
		setResizable(false);
		setMinimumSize(new Dimension(1200, 800));
		setFont(new Font("Cambria", Font.PLAIN, 18));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		 setLocationRelativeTo(null);
		setVisible(rendreVisible);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// action de recherche
		if ("Annuler".equals(e.getActionCommand())) {
			this.dispose();
			new IhmEcranAccueil();
		}
		if ("Rechercher".equals(e.getActionCommand())) {
			Client client = new Client();
			String choixSelection = "";
			
			if (this.buttonGroup.getSelection() != null) {
				choixSelection = this.buttonGroup.getSelection().getActionCommand();

				DtoClient dtoClient = new DtoClient();
			

				client = dtoClient.rechercheClient(choixSelection, txtSaisie.getText());
				if (client != null) {
					new IhmClient(client);
					this.dispose();
				}
				else if (client == null) {
				new IhmMessagePersonnalisable("Client introuvable");
				}

			}
			else {
			new IhmMessagePersonnalisable("Veuillez definir le type de recherche");
			}

		}
	}
}
