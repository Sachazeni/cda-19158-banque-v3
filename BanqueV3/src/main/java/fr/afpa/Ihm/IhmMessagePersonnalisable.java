package fr.afpa.Ihm;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class IhmMessagePersonnalisable extends JFrame implements ActionListener {
	private JLabel lblTitreErreur;
	private JTextField txtMessageIci;
	private JButton btnOk;

	public IhmMessagePersonnalisable(String message) {

		// creation Menu
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnUtilisateur = new JMenu("Utilisateur");
		menuBar.add(mnUtilisateur);

		JMenuItem mntmDeconnexion = new JMenuItem("Deconnexion");
		mnUtilisateur.add(mntmDeconnexion);

		JMenu mnAide = new JMenu("Aide");
		menuBar.add(mnAide);

		// creation Panel princpal
		JPanel contentPanePrincipal = new JPanel();
		contentPanePrincipal.setBackground(UIManager.getColor("Button.light"));
		contentPanePrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanePrincipal);
		contentPanePrincipal.setLayout(null);

		// le label à modiifer en fonction du titre du message

		lblTitreErreur = new JLabel("ERREUR !");
		lblTitreErreur.setFont(new Font("Cambria", Font.PLAIN, 18));
		lblTitreErreur.setBounds(137, 33, 169, 22);
		contentPanePrincipal.add(lblTitreErreur);

		// messsage à modifiier en fonction du ce qu'on veut afficher
		txtMessageIci = new JTextField();
		txtMessageIci.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		txtMessageIci.setHorizontalAlignment(SwingConstants.CENTER);
		txtMessageIci.setText(message);
		txtMessageIci.setBackground(Color.WHITE);
		txtMessageIci.setEditable(false);
		txtMessageIci.setBounds(43, 88, 357, 68);
		contentPanePrincipal.add(txtMessageIci);

		// init bouton ok
		btnOk = new JButton("OK");
		btnOk.setBounds(173, 189, 98, 26);
		btnOk.addActionListener(this);
		contentPanePrincipal.add(btnOk);

		// fenetre init
		setTitle("Banque CDA-V3");
		setResizable(false);
		setMinimumSize(new Dimension(400, 150));
		setFont(new Font("Cambria", Font.PLAIN, 18));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		 setLocationRelativeTo(null);
		 setAlwaysOnTop(true);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if ("OK".equals(e.getActionCommand())) {
			this.dispose();
		}

	}

}
