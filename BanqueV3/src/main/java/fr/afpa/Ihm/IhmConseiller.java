package fr.afpa.Ihm;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.Format;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

public class IhmConseiller extends JFrame implements ActionListener {

	private JTextField txtNom;
	private JFormattedTextField formTxtIdentifiant;
	private JFormattedTextField formCodeAg;
	private JButton btnValider;
	private JFormattedTextField fmtTxtIdMetier;
	private JButton btnAnnulerLaCreation;
	private boolean rendreVisible;

	public IhmConseiller() {
		rendreVisible = true;

		// Crea Menus
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnUtilisateur = new JMenu("Utilisateur");
		menuBar.add(mnUtilisateur);

		JMenuItem mntmDeconnexion = new JMenuItem("Deconnexion");
		mnUtilisateur.add(mntmDeconnexion);

		JMenu mnAide = new JMenu("Aide");
		menuBar.add(mnAide);

		// Creation du panel principal
		JPanel contentPanePrincipal = new JPanel();
		contentPanePrincipal.setBackground(Color.WHITE);
		contentPanePrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanePrincipal);
		contentPanePrincipal.setLayout(null);

		// creation panel avec les bordures
		JPanel panelAvecBordure = new JPanel();
		panelAvecBordure.setFont(new Font("Cambria", Font.PLAIN, 18));
		panelAvecBordure.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229), 2, true),
				"Creation Nouveau Conseiller", TitledBorder.LEFT, TitledBorder.TOP, null, new Color(51, 51, 51)));
		panelAvecBordure.setBounds(83, 136, 1028, 478);
		contentPanePrincipal.add(panelAvecBordure);
		panelAvecBordure.setLayout(null);

		// Creation des Labels
		JLabel lblNom = new JLabel("Nom :");
		lblNom.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNom.setFont(new Font("Cambria", Font.PLAIN, 18));
		lblNom.setBounds(190, 81, 46, 14);
		panelAvecBordure.add(lblNom);

		JLabel lblIdentifiant = new JLabel("Identifiant :");
		lblIdentifiant.setHorizontalAlignment(SwingConstants.RIGHT);
		lblIdentifiant.setLocation(11, 176);
		lblIdentifiant.setSize(225, 17);
		lblIdentifiant.setFont(new Font("Cambria", Font.PLAIN, 18));
		panelAvecBordure.add(lblIdentifiant);

		JLabel lblIdMetier = new JLabel("Identifiant Metier :");
		lblIdMetier.setHorizontalAlignment(SwingConstants.RIGHT);
		lblIdMetier.setLocation(11, 274);
		lblIdMetier.setSize(225, 17);
		lblIdMetier.setFont(new Font("Cambria", Font.PLAIN, 18));
		panelAvecBordure.add(lblIdMetier);

		JLabel lblCodeAgence = new JLabel("Code Agence :");
		lblCodeAgence.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCodeAgence.setLocation(128, 372);
		lblCodeAgence.setSize(108, 22);
		lblCodeAgence.setFont(new Font("Cambria", Font.PLAIN, 18));
		panelAvecBordure.add(lblCodeAgence);

		// init des zones de textes pour recup les données entrées
		txtNom = new JTextField();
		txtNom.setBounds(246, 81, 531, 21);
		panelAvecBordure.add(txtNom);
		txtNom.setColumns(10);
		txtNom.addActionListener(this);

		formTxtIdentifiant = new JFormattedTextField();
		formTxtIdentifiant.setColumns(10);
		formTxtIdentifiant.setBounds(246, 177, 531, 21);
		panelAvecBordure.add(formTxtIdentifiant);
		formTxtIdentifiant.addActionListener(this);

		formCodeAg = new JFormattedTextField((Format) null);
		formCodeAg.setBounds(246, 376, 529, 21);
		panelAvecBordure.add(formCodeAg);
		formCodeAg.addActionListener(this);

		fmtTxtIdMetier = new JFormattedTextField((Format) null);
		fmtTxtIdMetier.setBounds(246, 275, 529, 21);
		panelAvecBordure.add(fmtTxtIdMetier);
		fmtTxtIdMetier.addActionListener(this);
		
		//init des boutons
		btnValider = new JButton("Valider la creation");
		btnValider.setFont(new Font("Cambria", Font.PLAIN, 14));
		btnValider.setBounds(632, 440, 145, 28);
		panelAvecBordure.add(btnValider);
		btnValider.addActionListener(this);
		
		 btnAnnulerLaCreation = new JButton("Annuler la creation");
		btnAnnulerLaCreation.setFont(new Font("Cambria", Font.PLAIN, 14));
		btnAnnulerLaCreation.setBounds(11, 440, 151, 28);
		panelAvecBordure.add(btnAnnulerLaCreation);
		btnAnnulerLaCreation.addActionListener(this);
		
		//init fenetre
		setTitle("Banque CDA-V3");
		setResizable(false);
		setMinimumSize(new Dimension(1200, 800));
		setFont(new Font("Cambria", Font.PLAIN, 18));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setVisible(rendreVisible);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
//		if("Valider la creation".equals(e.getActionCommand())) {
//			
//		}
		if("Annuler la creation".equals(e.getActionCommand())) {
			this.dispose();
			new IhmEcranAccueil();
		}
		
	}

}
