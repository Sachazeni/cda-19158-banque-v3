package fr.afpa.Ihm;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import fr.afpa.Entite.Client;

public class IhmClient extends JFrame implements ActionListener{

	private JTextField txtNom;
	private JTextField txtPrenom;
	private JTextField txtIdentifiant;
	private JTextField txtMail;
	private JTextField txtDate;
	private JTextField txtCodeCons;
	private ButtonGroup buttonGroup = new ButtonGroup();
	private boolean rendreVisible = true;
	private Client client;

	public IhmClient(Client clientTrouve) {
		//Init Barre de menu
		this.client=clientTrouve;
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnUtilisateur = new JMenu("Utilisateur");
		menuBar.add(mnUtilisateur);

		JMenuItem mntmDeconnexion = new JMenuItem("Deconnexion");
		mnUtilisateur.add(mntmDeconnexion);

		JMenu mnAide = new JMenu("Aide");
		menuBar.add(mnAide);

		// creation Panel princpal
		JPanel contentPanePrincipal = new JPanel();
		contentPanePrincipal.setBackground(UIManager.getColor("Button.light"));
		contentPanePrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanePrincipal);
		contentPanePrincipal.setLayout(null);

		// Ajout bouton pour retourner à l'acceuil
		JButton btnRevenirAcceuil = new JButton("Retour Acceuil");
		btnRevenirAcceuil.setBounds(15, 548, 145, 26);
		contentPanePrincipal.add(btnRevenirAcceuil);
		btnRevenirAcceuil.addActionListener(this);

		// -------------- Creation d'un panel qui va contenir le titre et le label avec
		// le nom et prenom Client
		JPanel panelHautTitre = new JPanel();
		panelHautTitre.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "", TitledBorder.LEADING,
				TitledBorder.TOP, null, new Color(51, 51, 51)));
		panelHautTitre.setBounds(15, 18, 1155, 53);
		contentPanePrincipal.add(panelHautTitre);
		panelHautTitre.setLayout(null);
		
		//Label tout en haut avec le nom et prenom qu'il faudra modifier en fonction du client
		JLabel lblTitreNomClient = new JLabel(client.getNom()+" "+client.getPrenom());
		lblTitreNomClient.setBounds(5, 18, 211, 28);
		panelHautTitre.add(lblTitreNomClient);
		lblTitreNomClient.setForeground(Color.BLACK);
		lblTitreNomClient.setFont(new Font("Cambria", Font.BOLD, 23));

		// -------------- Creation d'un panel qui va afficher les informations du client
		JPanel panelDetails = new JPanel();
		panelDetails.setFont(new Font("Cambria", Font.PLAIN, 14));
		panelDetails.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Details : ",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelDetails.setBounds(15, 100, 769, 399);
		contentPanePrincipal.add(panelDetails);
		panelDetails.setLayout(null);
		
		//labels en face des zones de textes pour que le client sache ou il faut ajouter quoi
		JLabel lblNom = new JLabel("Nom :");
		lblNom.setForeground(Color.BLACK);
		lblNom.setBounds(116, 30, 48, 24);
		panelDetails.add(lblNom);
		lblNom.setFont(new Font("Cambria", Font.PLAIN, 19));

		JLabel lblPrenom = new JLabel("Prenom :");
		lblPrenom.setBounds(90, 68, 74, 24);
		panelDetails.add(lblPrenom);
		lblPrenom.setForeground(new Color(0, 0, 0));
		lblPrenom.setFont(new Font("Cambria", Font.PLAIN, 19));

		JLabel lblId = new JLabel("Identifiant :");
		lblId.setForeground(Color.BLACK);
		lblId.setBounds(70, 106, 94, 24);
		panelDetails.add(lblId);
		lblId.setFont(new Font("Cambria", Font.PLAIN, 19));

		JLabel lblMail = new JLabel("Mail :");
		lblMail.setForeground(Color.BLACK);
		lblMail.setFont(new Font("Cambria", Font.PLAIN, 19));
		lblMail.setBounds(120, 144, 44, 24);
		panelDetails.add(lblMail);

		JLabel lblDateDeNaissance = new JLabel("Date de naissance :");
		lblDateDeNaissance.setForeground(Color.BLACK);
		lblDateDeNaissance.setFont(new Font("Cambria", Font.PLAIN, 19));
		lblDateDeNaissance.setBounds(12, 182, 152, 24);
		panelDetails.add(lblDateDeNaissance);

		JLabel lblCodeConseiller = new JLabel("Code conseiller :");
		lblCodeConseiller.setForeground(Color.BLACK);
		lblCodeConseiller.setFont(new Font("Cambria", Font.PLAIN, 19));
		lblCodeConseiller.setBounds(32, 221, 132, 24);
		panelDetails.add(lblCodeConseiller);

		JLabel lblClientActif = new JLabel("Client actif :");
		lblClientActif.setForeground(Color.BLACK);
		lblClientActif.setFont(new Font("Cambria", Font.PLAIN, 19));
		lblClientActif.setBounds(70, 258, 94, 24);
		panelDetails.add(lblClientActif);
		
		//Zones de Textes 
		txtNom = new JTextField(client.getNom());
		txtNom.setFont(new Font("Dialog", Font.PLAIN, 14));
		lblNom.setLabelFor(txtNom);
		txtNom.setBounds(176, 33, 244, 20);
		panelDetails.add(txtNom);
		txtNom.setColumns(10);
		txtNom.addActionListener(this);
		
		txtPrenom = new JTextField(client.getPrenom());
		lblPrenom.setLabelFor(txtPrenom);
		txtPrenom.setFont(new Font("Dialog", Font.PLAIN, 14));
		txtPrenom.setColumns(10);
		txtPrenom.setBounds(176, 71, 244, 20);
		panelDetails.add(txtPrenom);
		txtPrenom.addActionListener(this);

		txtIdentifiant = new JTextField(client.getIdClient());
		lblId.setLabelFor(txtIdentifiant);
		txtIdentifiant.setFont(new Font("Dialog", Font.PLAIN, 14));
		txtIdentifiant.setColumns(10);
		txtIdentifiant.setBounds(176, 110, 244, 20);
		panelDetails.add(txtIdentifiant);
		txtIdentifiant.addActionListener(this);

		txtMail = new JTextField(client.getMail());
		txtMail.setFont(new Font("Dialog", Font.PLAIN, 14));
		txtMail.setColumns(10);
		txtMail.setBounds(176, 148, 244, 20);
		panelDetails.add(txtMail);
		txtMail.addActionListener(this);

		txtDate = new JTextField(client.getDateDeNaissance()+"");
		txtDate.setFont(new Font("Dialog", Font.PLAIN, 14));
		txtDate.setColumns(10);
		txtDate.setBounds(176, 186, 244, 20);
		panelDetails.add(txtDate);
		txtDate.addActionListener(this);

		txtCodeCons = new JTextField(client.getIdEmp());
		txtCodeCons.setFont(new Font("Dialog", Font.PLAIN, 14));
		txtCodeCons.setColumns(10);
		txtCodeCons.setBounds(176, 224, 244, 20);
		panelDetails.add(txtCodeCons);
		txtCodeCons.addActionListener(this);
		
		//boutons radion qui vont servir si jamais on veut desactiver un client
		JRadioButton rdbtnOui = new JRadioButton("Oui");
		buttonGroup.add(rdbtnOui);
		rdbtnOui.setSelected(true);
		rdbtnOui.setBounds(172, 260, 44, 24);
		panelDetails.add(rdbtnOui);

		JRadioButton rdbtnNon = new JRadioButton("Non");
		buttonGroup.add(rdbtnNon);
		rdbtnNon.setBounds(220, 260, 47, 24);
		panelDetails.add(rdbtnNon);

		// -------------- Creation d'un panel qui va contenir les boutons 
		JPanel panelEffectuerOp = new JPanel();
		panelEffectuerOp.setLayout(null);
		panelEffectuerOp.setFont(new Font("Cambria", Font.PLAIN, 14));
		panelEffectuerOp.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)),
				"Effectuer une operation : ", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelEffectuerOp.setBounds(818, 100, 352, 399);
		contentPanePrincipal.add(panelEffectuerOp);

		//-------Boutons dans le panel de droite 
		// sauvegarder les modifications s'il y a eu
		JButton btnSauvegarderModifications = new JButton("Sauvegarder les modifications du Client");
		btnSauvegarderModifications.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnSauvegarderModifications.setBounds(39, 55, 273, 26);
		panelEffectuerOp.add(btnSauvegarderModifications);

		//Bouton avec direction vers la gestion des comptes du client
		JButton btnGestionComptes = new JButton("Gestion des comptes du Client");
		btnGestionComptes.setBounds(39, 136, 273, 26);
		panelEffectuerOp.add(btnGestionComptes);
		btnGestionComptes.addActionListener(this);
		
		pack();
		
		setTitle("Banque CDA-V3");
		setResizable(false);
		setMinimumSize(new Dimension(1200, 800));
		setFont(new Font("Cambria", Font.PLAIN, 18));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setVisible(rendreVisible);
		 setLocationRelativeTo(null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//Sauvegarde des modification
		if("Retour Acceuil".equals(e.getActionCommand())) {
			this.dispose();
			new IhmEcranAccueil();
		}
		else if("Gestion des comptes du Client".equals(e.getActionCommand())) {
			if(client!=null) {
				
				new IhmListeComptesClients(client);
				this.dispose();
				
			}
			else {
				new IhmMessagePersonnalisable("Une erreur est survenue veuillez recommencer");
				
			}
		}
		
	}
}
