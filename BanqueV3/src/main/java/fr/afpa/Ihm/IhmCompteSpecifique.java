package fr.afpa.Ihm;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import fr.afpa.Entite.Client;
import lombok.Getter;
import lombok.Setter;
@Getter

public class IhmCompteSpecifique extends JFrame implements ActionListener {
	private Client client;
	
	public IhmCompteSpecifique(Client clTrouve) {
		client=clTrouve;
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnUtilisateur = new JMenu("Utilisateur");
		menuBar.add(mnUtilisateur);
		
		
		JMenuItem mntmDeconnexion = new JMenuItem("Deconnexion");
		mnUtilisateur.add(mntmDeconnexion);
		
		JMenuItem mntmAcceuil = new JMenuItem("Acceuil");
		mnUtilisateur.add(mntmAcceuil);
		
		JMenu mnAide = new JMenu("Aide");
		menuBar.add(mnAide);
		
		
		//creation Panel princpal
		JPanel contentPanePrincipal = new JPanel();
		contentPanePrincipal.setBackground(UIManager.getColor("Button.light"));
		contentPanePrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanePrincipal);
		contentPanePrincipal.setLayout(null);
		
		//Ajout bouton pour retourner à la fiche client
		JButton btnRevenirFicheClient = new JButton("Revenir Fiche Client");
		btnRevenirFicheClient.setBounds(15, 548, 145, 26);
		contentPanePrincipal.add(btnRevenirFicheClient);
		btnRevenirFicheClient.addActionListener(this);
		
		// -------------- Creation d'un panel qui va contenir le titre et le label avec le numero de compte ciblé
		JPanel panelHautTitre = new JPanel();
		panelHautTitre.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
		panelHautTitre.setBounds(15, 18, 1155, 53);
		contentPanePrincipal.add(panelHautTitre);
		panelHautTitre.setLayout(null);
		
		JLabel lblNumeroDeCompte = new JLabel("Numero de compte :");
		lblNumeroDeCompte.setBounds(5, 18, 211, 28);
		panelHautTitre.add(lblNumeroDeCompte);
		lblNumeroDeCompte.setForeground(Color.BLACK);
		lblNumeroDeCompte.setFont(new Font("Cambria", Font.BOLD, 23));
		
		//efectuer des changement sur ce label en fonction du numero de compte
		JLabel lblAfficherNumCompte = new JLabel("0123456789");
		lblAfficherNumCompte.setBounds(230, 19, 130, 29);
		panelHautTitre.add(lblAfficherNumCompte);
		lblAfficherNumCompte.setFont(new Font("Cambria", Font.PLAIN, 20));
		
		// -------------- Creation d'un panel qui va afficher les informations du compte (dans l'ordre des labels :
		//Type de compte -  Solde - Decouvert
		JPanel panelDetails = new JPanel();
		panelDetails.setFont(new Font("Cambria", Font.PLAIN, 14));
		panelDetails.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Details : ", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelDetails.setBounds(15, 100, 769, 168);
		contentPanePrincipal.add(panelDetails);
		panelDetails.setLayout(null);
		
		JLabel lblTypeDeCompte = new JLabel("Type de compte :");
		lblTypeDeCompte.setForeground(Color.BLACK);
		lblTypeDeCompte.setBounds(12, 24, 146, 24);
		panelDetails.add(lblTypeDeCompte);
		lblTypeDeCompte.setFont(new Font("Cambria", Font.PLAIN, 19));
		
		JLabel lblSolde = new JLabel("Solde :");
		lblSolde.setBounds(101, 72, 57, 24);
		panelDetails.add(lblSolde);
		lblSolde.setForeground(new Color(0, 0, 0));
		lblSolde.setFont(new Font("Cambria", Font.PLAIN, 19));
		
		JLabel lblDcouvert = new JLabel("Découvert :");
		lblDcouvert.setForeground(Color.BLACK);
		lblDcouvert.setBounds(59, 120, 99, 24);
		panelDetails.add(lblDcouvert);
		lblDcouvert.setFont(new Font("Cambria", Font.PLAIN, 19));
		
		//Labels sur les quels faudra changer le texte en fonction du compte ciblée :
		// Dans l'ordre d'appartenannce : Type de compte -  Solde - Decouvert
		JLabel lblCompteCourant = new JLabel("Compte Courant");
		lblCompteCourant.setBounds(161, 27, 114, 20);
		panelDetails.add(lblCompteCourant);
		lblCompteCourant.setFont(new Font("Cambria", Font.PLAIN, 16));
		
		JLabel lblSoldeici = new JLabel("soldeIci");
		lblSoldeici.setBounds(161, 74, 114, 20);
		panelDetails.add(lblSoldeici);
		lblSoldeici.setFont(new Font("Cambria", Font.PLAIN, 16));
		
		JLabel lblAutoris = new JLabel("Autorisé");
		lblAutoris.setFont(new Font("Cambria", Font.PLAIN, 16));
		lblAutoris.setBounds(161, 121, 114, 20);
		panelDetails.add(lblAutoris);
		
		
		// -------------- Creation d'un panel qui va contenir  les elements pour la modification du decouvert autorisé et son etat(activer-desactiver)
		JPanel panelApportModif = new JPanel();
		panelApportModif.setForeground(Color.BLACK);
		panelApportModif.setLayout(null);
		panelApportModif.setFont(new Font("Cambria", Font.PLAIN, 14));
		panelApportModif.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Apporter des modifications : ", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelApportModif.setBounds(15, 302, 769, 197);
		contentPanePrincipal.add(panelApportModif);
		
		JLabel lblautorisationDecouvert = new JLabel("Changer l'autorisation de découvert :");
		lblautorisationDecouvert.setForeground(Color.DARK_GRAY);
		lblautorisationDecouvert.setFont(new Font("Cambria", Font.BOLD, 16));
		lblautorisationDecouvert.setBounds(12, 29, 289, 21);
		panelApportModif.add(lblautorisationDecouvert);
		
		JComboBox comboBoxDecouvert = new JComboBox();
		comboBoxDecouvert.setModel(new DefaultComboBoxModel(new String[] {"Autorisé", "Non-Autorisé"}));
		comboBoxDecouvert.setBounds(12, 64, 144, 21);
		panelApportModif.add(comboBoxDecouvert);
		
		JLabel lblChangerLtatDu = new JLabel("Changer l'état du compte :");
		lblChangerLtatDu.setForeground(Color.DARK_GRAY);
		lblChangerLtatDu.setFont(new Font("Cambria", Font.BOLD, 16));
		lblChangerLtatDu.setBounds(12, 99, 289, 21);
		panelApportModif.add(lblChangerLtatDu);
		
		JComboBox comboBoxEtatCompte = new JComboBox();
		comboBoxEtatCompte.setModel(new DefaultComboBoxModel(new String[] {"Actif", "Fermé"}));
		comboBoxEtatCompte.setBounds(12, 134, 144, 21);
		panelApportModif.add(comboBoxEtatCompte);
		
		//bouton avec action Listener pour confirmer les modif effectués
		
		JButton btnConfirmerLesModifications = new JButton("Confirmer les modifications");
		btnConfirmerLesModifications.setBounds(566, 159, 191, 26);
		panelApportModif.add(btnConfirmerLesModifications);
		btnConfirmerLesModifications.addActionListener(this);
		
		// -------------- Creation d'un panel qui va contenir  les boutons pour effectuer les operations:
		// Dans l'ordre Impression historique- Alimenter compte-Retrait-Virement-Activer/desactiver le compte
		JPanel panelEffectuerOp = new JPanel();
		panelEffectuerOp.setLayout(null);
		panelEffectuerOp.setFont(new Font("Cambria", Font.PLAIN, 14));
		panelEffectuerOp.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Effectuer une operation : ", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelEffectuerOp.setBounds(818, 100, 352, 399);
		contentPanePrincipal.add(panelEffectuerOp);
		
		JButton btnImprimerLhistoriqueDes = new JButton("Imprimer l'historique des operations");
		btnImprimerLhistoriqueDes.setBounds(39, 55, 273, 26);
		panelEffectuerOp.add(btnImprimerLhistoriqueDes);
		btnImprimerLhistoriqueDes.addActionListener(this);
		
		JButton btnAlimenterLeCompte = new JButton("Alimenter le compte");
		btnAlimenterLeCompte.setBounds(39, 136, 273, 26);
		panelEffectuerOp.add(btnAlimenterLeCompte);
		btnAlimenterLeCompte.addActionListener(this);
		
		JButton btnRetraitDuneSomme = new JButton("Retrait d'une somme");
		btnRetraitDuneSomme.setBounds(39, 217, 273, 26);
		panelEffectuerOp.add(btnRetraitDuneSomme);
		btnRetraitDuneSomme.addActionListener(this);
				
		JButton btnEffectuerUnVirement = new JButton("Effectuer un virement");
		btnEffectuerUnVirement.setBounds(39, 298, 273, 26);
		panelEffectuerOp.add(btnEffectuerUnVirement);
		setTitle("Banque CDA-V3");
		setResizable(false);
		setMinimumSize(new Dimension(1200, 800));
		setFont(new Font("Cambria", Font.PLAIN, 18));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setVisible(true);
		btnEffectuerUnVirement.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
//		if("Imprimer l'historique des operations".equals(e.getActionCommand())) {
//			
//		}
		if("Alimenter le compte".equals(e.getActionCommand())) {
			new IhmOperationsSimples();
		}
		if("Retrait d'une somme".equals(e.getActionCommand())) {
			new IhmOperationsSimples();			
		}
		if("Effectuer un virement".equals(e.getActionCommand())) {
			new IhmOperationVirement();
		}
//		if("Confirmer les modifications".equals(e.getActionCommand())) {
//			this.dispose();
//			new IhmClient();
//		}
		if("Revenir Fiche Client".equals(e.getActionCommand())) {
			this.dispose();
			new IhmClient(client);
		}
		
	}
}
