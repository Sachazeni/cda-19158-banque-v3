package fr.afpa.Ihm;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.text.DateFormatter;

import fr.afpa.Dto.DtoClient;

public class IhmCreationNouveauClient extends JFrame implements ActionListener {
	// declaration des variables

	private JTextField txtNom;
	private JTextField txtPrenom;
	private JFormattedTextField formTxtIdentifiant;
	private JTextField txtMail;
	private JTextField TxtDate;
	private JFormattedTextField formTextCons;
	private JComboBox comboBoxActif;
	private JButton btnValiderEtPasser;
	private JButton btnAnnuler;
	boolean actif;

	// constructeur
	public IhmCreationNouveauClient() {

		setTitle("Banque CDA-V3");
		setResizable(false);
		setMinimumSize(new Dimension(1200, 800));
		setFont(new Font("Cambria", Font.PLAIN, 18));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setVisible(true);

		// creation barre de menu
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		// creation onglet Utilisateur dans la barre de menu
		JMenu mnUtilisateur = new JMenu("Utilisateur");
		menuBar.add(mnUtilisateur);

		// Creation des items dans le menu deroulant de l'utilisateur
		JMenuItem mntmAcceuil = new JMenuItem("Acceuil");
		mnUtilisateur.add(mntmAcceuil);
		JMenuItem mntmDeconnexion = new JMenuItem("Deconnexion");
		mnUtilisateur.add(mntmDeconnexion);

		// creation onglet Aide dans la barre de menu
		JMenu mnAide = new JMenu("Aide");
		menuBar.add(mnAide);

		// Creation PanelPrincipal
		JPanel contentPanePrincipal = new JPanel();
		contentPanePrincipal.setBackground(Color.WHITE);
		contentPanePrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanePrincipal);
		contentPanePrincipal.setLayout(null);

		// Creation panel qui va contenir l'ensemble des labels textfiels et autres ,
		// possede une bordure avec un titre
		JPanel panelAvecBordure = new JPanel();
		panelAvecBordure.setFont(new Font("Cambria", Font.PLAIN, 18));
		panelAvecBordure.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229), 2, true),
				"Creation Nouveau Client", TitledBorder.LEFT, TitledBorder.TOP, null, new Color(51, 51, 51)));
		panelAvecBordure.setBounds(83, 136, 1028, 478);
		contentPanePrincipal.add(panelAvecBordure);
		panelAvecBordure.setLayout(null);

		// Creation des labels devants les text fields pour indiquer à quoi ils
		// correspondent :

		JLabel lblNom = new JLabel("Nom :");
		lblNom.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNom.setFont(new Font("Cambria", Font.PLAIN, 18));
		lblNom.setBounds(190, 47, 46, 14);
		panelAvecBordure.add(lblNom);

		JLabel lblPrenom = new JLabel("Prenom :");
		lblPrenom.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPrenom.setFont(new Font("Cambria", Font.PLAIN, 18));
		lblPrenom.setBounds(166, 108, 70, 14);
		panelAvecBordure.add(lblPrenom);

		JLabel lblIdentifiant = new JLabel("Identifiant :");
		lblIdentifiant.setHorizontalAlignment(SwingConstants.RIGHT);
		lblIdentifiant.setLocation(11, 169);
		lblIdentifiant.setSize(225, 17);
		lblIdentifiant.setFont(new Font("Cambria", Font.PLAIN, 18));
		panelAvecBordure.add(lblIdentifiant);

		JLabel lblMail = new JLabel("Mail :");
		lblMail.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMail.setLocation(11, 230);
		lblMail.setSize(225, 17);
		lblMail.setFont(new Font("Cambria", Font.PLAIN, 18));
		panelAvecBordure.add(lblMail);

		JLabel lblDateNais = new JLabel("Date de naissance  :");
		lblDateNais.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDateNais.setLocation(11, 291);
		lblDateNais.setSize(225, 17);
		lblDateNais.setFont(new Font("Cambria", Font.PLAIN, 18));
		panelAvecBordure.add(lblDateNais);

		JLabel lblCodeCons = new JLabel("Code du conseiller attribué :");
		lblCodeCons.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCodeCons.setLocation(11, 345);
		lblCodeCons.setSize(225, 17);
		lblCodeCons.setFont(new Font("Cambria", Font.PLAIN, 18));
		panelAvecBordure.add(lblCodeCons);

		JLabel lblActif = new JLabel("Client actif :");
		lblActif.setHorizontalAlignment(SwingConstants.RIGHT);
		lblActif.setLocation(11, 413);
		lblActif.setSize(225, 17);
		lblActif.setFont(new Font("Cambria", Font.PLAIN, 18));
		panelAvecBordure.add(lblActif);

		// Textfield qui va contenir le nom
		txtNom = new JTextField();
		txtNom.setBounds(255, 46, 531, 21);
		panelAvecBordure.add(txtNom);
		txtNom.setColumns(10);
		txtNom.addActionListener(this);

		// Textfield qui va contenir le prenom
		txtPrenom = new JTextField();
		txtPrenom.setColumns(10);
		txtPrenom.setBounds(255, 108, 531, 21);
		panelAvecBordure.add(txtPrenom);
		txtPrenom.addActionListener(this);

		// Textfield formatte pour pouvoir faire un premier masque de controle si
		// l'utilisateur rentre bien des chiffres
		formTxtIdentifiant = new JFormattedTextField();
		formTxtIdentifiant.setColumns(10);
		formTxtIdentifiant.setBounds(255, 169, 531, 21);
		panelAvecBordure.add(formTxtIdentifiant);
		formTxtIdentifiant.addActionListener(this);

		// Textfield formatte pour pouvoir faire un premier masque de controle si
		// l'utilisateur rentre bien un mail
		txtMail = new JTextField();
		txtMail.setColumns(10);
		txtMail.setBounds(255, 231, 531, 21);
		panelAvecBordure.add(txtMail);
		txtMail.addActionListener(this);

		// Textfield formatte pour pouvoir faire un premier masque de controle si
		// l'utilisateur rentre bien une date

		TxtDate = new JTextField();
		TxtDate.setBounds(255, 292, 529, 21);
		TxtDate.setText(TxtDate.getText());
		panelAvecBordure.add(TxtDate);
		

		// Textfield formatte pour pouvoir faire un premier masque de controle si
		// l'utilisateur rentre bien des chiffres
		formTextCons = new JFormattedTextField((Format) null);
		formTextCons.setBounds(255, 346, 529, 21);
		panelAvecBordure.add(formTextCons);
		formTextCons.addActionListener(this);

		// comboBox pour selectioner si le client est actif ou pas
		comboBoxActif = new JComboBox();
		comboBoxActif.setFont(new Font("Tahoma", Font.PLAIN, 16));
		comboBoxActif.setModel(new DefaultComboBoxModel(new String[] { "Oui", "Non" }));
		comboBoxActif.setBounds(257, 413, 56, 20);
		panelAvecBordure.add(comboBoxActif);
		if("Oui".equals(comboBoxActif.getSelectedItem().toString())){
			actif=true;
		}
		else {
			actif=false;
		}
		// bouton pour valider la creation du nouveau client et passer à la creation du
		// compte
		btnValiderEtPasser = new JButton("Valider");
		btnValiderEtPasser.setFont(new Font("Cambria", Font.PLAIN, 14));
		btnValiderEtPasser.setBounds(436, 413, 350, 23);
		panelAvecBordure.add(btnValiderEtPasser);
		btnValiderEtPasser.addActionListener(this);

		// bouton pour annuler la creation du nouveau client et revenir à l'ecran
		// d'acceuil
		btnAnnuler = new JButton("Annuler");
		btnAnnuler.setFont(new Font("Cambria", Font.PLAIN, 14));
		btnAnnuler.setBounds(800, 413, 100, 23);
		panelAvecBordure.add(btnAnnuler);
		btnAnnuler.addActionListener(this);

		setTitle("Banque CDA-V3");
		setResizable(false);
		setMinimumSize(new Dimension(1200, 800));
		setFont(new Font("Cambria", Font.PLAIN, 18));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setVisible(true);
		setLocationRelativeTo(null);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// action creation de client
		if ("Annuler".equals(e.getActionCommand())) {
			this.dispose();
			new IhmEcranAccueil();

		}
		if ("Valider".equals(e.getActionCommand())) {
		
			DtoClient dtoCl = new DtoClient();
			try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yy");
		    LocalDate  date = LocalDate.parse(TxtDate.getText().toUpperCase(), formatter);
		    
		    if(dtoCl.initClient(formTxtIdentifiant.getText(),txtNom.getText(), txtPrenom.getText(), date,
			txtMail.getText(), actif, formTextCons.getText())) {
		    	this.dispose();
		    	new IhmEcranAccueil();
		    	IhmMessagePersonnalisable ihmSucces= new IhmMessagePersonnalisable("Le client à bien été crée");
		    	ihmSucces.getLblTitreErreur().setText("Client crée");
		    	this.dispose();
		    	new IhmEcranAccueil();
		    }
		    
			}catch(DateTimeParseException ex) {
				new IhmMessagePersonnalisable("La date doit avoir le format: dd/MM/yy ");
				
			}
	
		}

	}
}
