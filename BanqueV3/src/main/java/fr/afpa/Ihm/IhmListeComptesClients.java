package fr.afpa.Ihm;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;

import fr.afpa.Dto.DtoComptes;
import fr.afpa.Entite.Client;

public class IhmListeComptesClients extends JFrame implements ActionListener{
	private boolean rendreVisible;
	List<String> listeC;
	
	private Client client;
	
	public IhmListeComptesClients(Client clientTrouve) {
		
		DtoComptes dtoC=new DtoComptes();
		
		rendreVisible=true;
		
		client=clientTrouve;
		listeC=dtoC.rechListe(client);
		//crea Menus
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnUtilisateur = new JMenu("Utilisateur");
		menuBar.add(mnUtilisateur);
		
		JMenuItem mntmDeconnexion = new JMenuItem("Deconnexion");
		mnUtilisateur.add(mntmDeconnexion);
		
		JMenu mnAide = new JMenu("Aide");
		menuBar.add(mnAide);
		
		
		//creation Panel princpal
		JPanel contentPanePrincipal = new JPanel();
		contentPanePrincipal.setBackground(UIManager.getColor("Button.light"));
		contentPanePrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanePrincipal);
		contentPanePrincipal.setLayout(null);
		
		//Ajout bouton pour retourner à la fiche client
		JButton btnRevenirAcceuil = new JButton("Retour Acceuil");
		btnRevenirAcceuil.setBounds(15, 548, 145, 26);
		contentPanePrincipal.add(btnRevenirAcceuil);
		btnRevenirAcceuil.addActionListener(this);
		
		// -------------- Creation d'un panel qui va contenir le titre avec le nom du client
		JPanel panelHautTitre = new JPanel();
		panelHautTitre.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
		panelHautTitre.setBounds(15, 18, 1155, 53);
		contentPanePrincipal.add(panelHautTitre);
		panelHautTitre.setLayout(null);
		
		//label à modifier en fonction du client
		JLabel lblTitreNomClient = new JLabel(""+client.getNom()+" "+client.getPrenom());
		lblTitreNomClient.setBounds(5, 18, 143, 28);
		panelHautTitre.add(lblTitreNomClient);
		lblTitreNomClient.setForeground(Color.BLACK);
		lblTitreNomClient.setFont(new Font("Cambria", Font.BOLD, 23));
		
		// -------------- Creation d'un panel qui va afficher la liste		et les boutons en lien
		JPanel panelDetails = new JPanel();
		panelDetails.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		panelDetails.setFont(new Font("Cambria", Font.PLAIN, 14));
		panelDetails.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Liste des Comptes bancaires du Client :", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelDetails.setBounds(15, 100, 769, 399);
		contentPanePrincipal.add(panelDetails);
		panelDetails.setLayout(null);
		
		//creation d'un panel avec le scrollbar pour faire defiler les comptes
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 30, 745, 206);
		panelDetails.add(scrollPane);
		
		//liste à initialiser avec les parametres recuperes
		// voir texte exemple pour les données à afficher
		JList listeComptes = new JList();
		scrollPane.setViewportView(listeComptes);
		listeComptes.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		listeComptes.setSelectedIndices(new int[] {0});
		DefaultListModel<String> model = new DefaultListModel<String>();
		for(String s:listeC){
		    model.addElement(s);
		}
		listeComptes.setModel(model);
		listeComptes.setSelectedIndex(-1);
		listeComptes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		JButton btnValiderChoixCompte = new JButton("Aller vers le compte selectionné");
		btnValiderChoixCompte.setBounds(541, 248, 216, 26);
		panelDetails.add(btnValiderChoixCompte);
		btnValiderChoixCompte.addActionListener(this);
		
		
		// -------------- Creation d'un panel qui va contenir  les boutons pour effectuer les operations:
		JPanel panelEffectuerOp = new JPanel();
		panelEffectuerOp.setLayout(null);
		panelEffectuerOp.setFont(new Font("Cambria", Font.PLAIN, 14));
		panelEffectuerOp.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Effectuer une operation : ", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelEffectuerOp.setBounds(818, 100, 352, 399);
		contentPanePrincipal.add(panelEffectuerOp);
		
		//bouton pour ouvrir un nouveau compte
		JButton btnOuvrirCompte = new JButton("Ouvrir un Nouveau Compte");
		btnOuvrirCompte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnOuvrirCompte.setBounds(39, 55, 273, 26);
		panelEffectuerOp.add(btnOuvrirCompte);
		btnOuvrirCompte.addActionListener(this);
		
		//bouton pour revenir vers la fiche du client
		JButton btnRetourFicheClient = new JButton("Retour Fiche Client");
		btnRetourFicheClient.setBounds(172, 548, 145, 26);
		contentPanePrincipal.add(btnRetourFicheClient);
		btnRetourFicheClient.addActionListener(this);
		pack();
				
		setTitle("Banque CDA-V3");
		setResizable(false);
		setMinimumSize(new Dimension(1200, 800));
		setFont(new Font("Cambria", Font.PLAIN, 18));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setVisible(rendreVisible);
		 setLocationRelativeTo(null);
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		//Aller vers le compte selectionné
		if("Retour Acceuil".equals(e.getActionCommand())) {
			this.dispose();
			new IhmEcranAccueil();
		}else if("Aller vers le compte selectionné".equals(e.getActionCommand())) {
			this.dispose();
			new IhmCompteSpecifique(client);
		}else if("Ouvrir un Nouveau Compte".equals(e.getActionCommand())) {
			new IhmCreationCompteB(client);
			this.dispose();
			new IhmCreationCompteB(client);
		}else if("Retour Fiche Client".equals(e.getActionCommand())){
			this.dispose();
			new IhmClient(client);
		}
		
	}
}
