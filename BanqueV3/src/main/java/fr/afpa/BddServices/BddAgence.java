package fr.afpa.BddServices;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import fr.afpa.Entite.Agence;

public class BddAgence {


public  boolean creationAgence(Connection con, Agence agence) {
	
	ResultSet resultats = null; // Requette à executer
	String requete = "INSERT INTO agences VALUES(?,?,?)";
	try {
		PreparedStatement pstmt =con.prepareStatement(requete);
		pstmt.setString(1, agence.getCodeAgence());
		pstmt.setString(2, agence.getNomAgence());
		pstmt.setString(3, agence.getAdresse());
		pstmt.executeUpdate();
		return true;
	} catch (SQLException e) {
		System.out.println(e.getMessage());
		return false;
	}
	
}
	
}

