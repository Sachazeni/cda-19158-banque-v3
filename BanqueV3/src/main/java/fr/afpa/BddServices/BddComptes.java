package fr.afpa.BddServices;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.afpa.Entite.Agence;
import fr.afpa.Entite.Client;
import fr.afpa.Entite.CompteBancaires;

public class BddComptes {

	public  boolean creationCompte(Connection con,Client client, String codeAg,CompteBancaires compte) {
		
		
		String requete = "INSERT INTO comptes_bancaires VALUES(?,?,?, ?,?,?,?)";
		try {
			PreparedStatement pstmt =con.prepareStatement(requete);
			
			pstmt.setLong(1, compte.getIdCompte());
			pstmt.setBoolean(2, compte.isDecouvert());
			pstmt.setBoolean(3, compte.isActif());
			pstmt.setDouble(4, compte.getSolde());
			pstmt.setInt(5, compte.getIdTypeCompte());
			pstmt.setString(6, client.getIdClient());
			pstmt.setString(7,codeAg);
	
			pstmt.executeUpdate();
			
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
		
	}
	public boolean alimCompte(Connection con, CompteBancaires compte) {
		String requete = "UPDATE compte_bancaires SET solde=? WHERE id=?";
				
				
				try {
					PreparedStatement pstmt =con.prepareStatement(requete);
					
					pstmt.setDouble(1, compte.getSolde());
					pstmt.setLong(2, compte.getIdCompte());
					
					pstmt.executeUpdate();
					
					return true;
				} catch (SQLException e) {
					System.out.println(e.getMessage());
					return false;
				}
	}
	
	public List<String> recupListeComptes(Connection con,Client client ){
		List<String>liste=new ArrayList<String>();
		ResultSet rs=null;
		String type="";
		Double solde;
		
		String requete ="SELECT tc.nom_type, cb.solde FROM comptes_bancaires cb INNER"
				+ " JOIN type_compte tc on tc.id_type_compte=cb.id_type_compte WHERE cb.id_client LIKE ?";
		try {
			PreparedStatement pstmt =con.prepareStatement(requete);
			pstmt.setString(1, client.getIdClient());
		
			rs = pstmt.executeQuery();
			while(rs.next()) {
				 type=rs.getString("nom_type");
				 solde=rs.getDouble("solde");
				 liste.add(type+" "+solde);
			}
		
			return liste;
		
	
	}catch (Exception e) {
		return null;
	}
}
	
	
	
	
	
	
	
}
