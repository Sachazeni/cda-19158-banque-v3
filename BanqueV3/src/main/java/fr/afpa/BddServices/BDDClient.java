package fr.afpa.BddServices;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDate;

import fr.afpa.Entite.Client;

public class BDDClient {
	
	public  boolean creationClient(Connection con,Client client) {
		
		
		String requete = "INSERT INTO clients VALUES(?,?,?,?,?,?,?)";
		try {
			PreparedStatement pstmt =con.prepareStatement(requete);
			pstmt.setString(1, client.getIdClient());
			pstmt.setString(2, client.getNom());
			pstmt.setString(3, client.getPrenom());
			pstmt.setDate(4, java.sql.Date.valueOf(client.getDateDeNaissance()));
			pstmt.setBoolean(5, client.isActif());
			pstmt.setString(6, client.getMail());
			pstmt.setString(7, client.getIdEmp());
			pstmt.executeUpdate();
			
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
		
	}
	
	public Client rechercheClient(Connection conn,String recherchePar,String donnees) {
		
		Client client=null;
		ResultSet resultats = null; // Requette à executer
		String requete="";
		if("nom".equals(recherchePar)) {
			
			requete = "SELECT * FROM clients WHERE nom = "+"'"+donnees+"'";
		}
		else if("identifiant".equals(recherchePar)) {
			requete = "SELECT * FROM clients WHERE id_client LIKE "+"'"+donnees+"'";
		}
		
		try {
			PreparedStatement pstmt =conn.prepareStatement(requete);
			
			resultats=pstmt.executeQuery();
			
			while(resultats.next()) {
				
			String id=resultats.getString(1);
			String nomCl =resultats.getString(2);
			String prenom=resultats.getString(3);
			Timestamp createdDate=resultats.getTimestamp(4);
			LocalDate dateNaiss= createdDate.toLocalDateTime().toLocalDate();
			boolean actif=resultats.getBoolean(5);
			String mail=resultats.getString(6);
			String idEmp = resultats.getString(7);
			
		    client=new Client(id, nomCl, prenom,dateNaiss,mail, actif, idEmp);
		
			}
			return client;	
		} catch (SQLException e) {
	
			return null;
		}
		
	}
}
