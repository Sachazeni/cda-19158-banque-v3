package fr.afpa.BddServices;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class BddConnexion {
	
	public static Connection connection() {
		Connection conn =null;
		String driverName = "org.postgresql.Driver";

		try {
			Class.forName(driverName);
			String url = "jdbc:postgresql://localhost:5432/BanqueV3";

			Properties props = new Properties();

			props.setProperty("user", "userB3");
			props.setProperty("password", "azerty");
			conn = DriverManager.getConnection(url, props);
			
			return conn;
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Erreur de connexion");
			return conn;

		} 
	}
}
