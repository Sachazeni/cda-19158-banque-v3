package fr.afpa.BddServices;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import fr.afpa.Ihm.IhmMessagePersonnalisable;

public class BddAuthentification {

	
	
	public static boolean creeConnexion(Connection con , String login , String mdp ) {
		ResultSet mdpEmp = null;
		String pwdEmp ="";
		String requete = "SELECT mdp_emp FROM employes where login_emp = (?)";
				try {
					PreparedStatement pstmt =con.prepareStatement(requete);
					pstmt.setString(1,login);
					mdpEmp = pstmt.executeQuery();
					
					while(mdpEmp.next()) {
						pwdEmp = mdpEmp.getString(1);
					}
					if(mdp.equals(pwdEmp)) {
						return true;
					}
				} catch (SQLException e) {
					new IhmMessagePersonnalisable(e.getMessage());
					System.out.println(e.getMessage());
				}
				return false;
	}
}
