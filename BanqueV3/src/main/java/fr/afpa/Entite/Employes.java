package fr.afpa.Entite;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level=AccessLevel.PRIVATE)
@EqualsAndHashCode
@ToString(of= {"idEmp","nom","loginEmp","mdpEmp","CodeAgence","IdMetier"})

public class Employes {

	String idEmp;
	String nom;
	String loginEmp;
	String mdpEmp;
	int CodeAgence;
	int IdMetier;
	
}
