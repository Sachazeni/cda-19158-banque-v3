package fr.afpa.Entite;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level=AccessLevel.PRIVATE)
@EqualsAndHashCode
@ToString(of= {"idCompte","decouvert","actif","solde","idTypeCompte"}) 
	
public class CompteBancaires {

	int idCompte;
	boolean decouvert;
	boolean actif;
	double solde;
	int idTypeCompte;
	
	public CompteBancaires() {
		super();
		// TODO Auto-generated constructor stub
	}
	public CompteBancaires(int idCompte, boolean decouvert, boolean actif, double solde, int idTypeCompte) {
		super();
		this.idCompte = idCompte;
		this.decouvert = decouvert;
		this.actif = actif;
		this.solde = solde;
		this.idTypeCompte = idTypeCompte;
	}

}
