package fr.afpa.Entite;

import java.time.LocalDate;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level=AccessLevel.PRIVATE)
@EqualsAndHashCode
@ToString(of= {"idClient","nom","prenom","dateDeNaissance","mail","actif"})
public class Client {
	
	String idClient;
	String nom;
	String prenom;
	LocalDate dateDeNaissance;
	String mail;
	boolean actif;
	String idEmp;
	
	public Client() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Client(String idClient, String nom, String prenom, LocalDate dateDeNaissance, String mail, boolean actif , String idEmp) {
		super();
		this.idClient = idClient;
		this.nom = nom;
		this.prenom = prenom;
		this.dateDeNaissance = dateDeNaissance;
		this.mail = mail;
		this.actif = actif;
		this.idEmp = idEmp;
	}
	
}
