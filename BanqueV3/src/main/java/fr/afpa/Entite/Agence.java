package fr.afpa.Entite;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level=AccessLevel.PRIVATE)
@EqualsAndHashCode
@ToString(of= {"codeAgence","nomAgence","adresse"})

public class Agence {

	String codeAgence;
	String nomAgence;
	String adresse;
	
	public Agence() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Agence(String codeAgence, String nomAgence, String adresse) {
		super();
		this.codeAgence = codeAgence;
		this.nomAgence = nomAgence;
		this.adresse = adresse;
	}
	
}
