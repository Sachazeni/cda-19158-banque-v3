package fr.afpa.Entite;

import java.time.LocalDate;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level=AccessLevel.PRIVATE)
@EqualsAndHashCode
@ToString(of= {"idOperartion","date","compteDestinataire","montant","idOp","IdCompte","IdClient"})

public class Operations {
	
	int idOperartion;
	LocalDate date;
	int compteDestinataire;
	double montant;
	int idOp;
	int IdCompte;
	String IdClient;
}

