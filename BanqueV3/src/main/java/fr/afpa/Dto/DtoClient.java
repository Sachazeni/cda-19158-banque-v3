package fr.afpa.Dto;

import java.sql.Connection;
import java.time.LocalDate;

import fr.afpa.BddServices.BDDClient;
import fr.afpa.BddServices.BddConnexion;
import fr.afpa.Entite.Client;

public class DtoClient {

	public  boolean initClient(String idClient, String nom, String prenom, LocalDate dateDeNaissance, String mail,
			boolean actif, String idEmp) {
		
	BDDClient envoisDonnesClient = new BDDClient();
	
	Client client = new Client(idClient, nom, prenom, dateDeNaissance, mail, actif, idEmp);	
	Connection connexionBdd = BddConnexion.connection();
	
	return envoisDonnesClient.creationClient(connexionBdd, client);
		
	}
	/**
	 * Methode pour appeler les methodes de la connexion et de celle qui va rechercher le client dans la base de données
	 * @param recherchePar : le rabiobutton Selectionnée
	 * @param donnees : le texte saisie
	 * @return le client trouvé
	 */
	public Client rechercheClient(String recherchePar, String donnees) {
		BDDClient requetesClient = new BDDClient();
		Connection connexionBdd = BddConnexion.connection();
		
		return  requetesClient.rechercheClient(connexionBdd, recherchePar, donnees);
	
}
	
}
