package fr.afpa.Dto;

import java.sql.Connection;
import java.util.List;

import fr.afpa.BddServices.BddComptes;
import fr.afpa.BddServices.BddConnexion;
import fr.afpa.Entite.Client;
import fr.afpa.Entite.CompteBancaires;
import fr.afpa.Services.ClientServices;

public class DtoComptes {
	
	public  boolean CreationComptes(Client client, String codeAg,CompteBancaires compte ) {
		
	BddComptes bddComptes = new BddComptes();
	Connection connexionBdd = BddConnexion.connection();
	
	return bddComptes.creationCompte(connexionBdd, client, codeAg, compte);
	}
	
	
	
	 public boolean alimCompte(CompteBancaires compte, Double montant) {
		 ClientServices clServ= new ClientServices();
		 BddComptes bddComptes = new BddComptes();
		 Connection connexionBdd = BddConnexion.connection();
		if( clServ.crediterCompte(compte,montant)) {
		 return	bddComptes.alimCompte(connexionBdd, compte);
		}
		else return false;
		 
	 }
	 public List<String> rechListe(Client client){
		 
		 BddComptes bddComptes = new BddComptes();
		 Connection connexionBdd = BddConnexion.connection();
		 return bddComptes.recupListeComptes(connexionBdd, client);
	 }
}
