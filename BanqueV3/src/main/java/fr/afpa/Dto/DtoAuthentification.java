package fr.afpa.Dto;

import java.sql.Connection;

import fr.afpa.BddServices.BddAuthentification;
import fr.afpa.BddServices.BddConnexion;

public class DtoAuthentification {

	public static boolean connexionEmp(String login, String mdp) {
		if (login != null && mdp != null) {
			Connection con= BddConnexion.connection();
			BddAuthentification.creeConnexion(con, login, mdp);
			return true;
		}
		return false;
	}
}
