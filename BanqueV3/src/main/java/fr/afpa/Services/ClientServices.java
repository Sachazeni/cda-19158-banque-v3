package fr.afpa.Services;

import java.time.LocalDate;

import fr.afpa.Entite.Client;

import fr.afpa.Entite.CompteBancaires;

public class ClientServices {


	/**
	 * methode de credit du compte
	 * 
	 * @param compte  le compte ciblé
	 * @param montant la somme a ajouter
	 * @return un boolean pour verifivation
	 */
	public static boolean crediterCompte(CompteBancaires compte, double montant) {
		if (compte != null && montant != 0) {
			compte.setSolde(compte.getSolde() + montant);
			return true;
		}
		return false;
	}

	/**
	 * methode pour debiter un compte
	 * 
	 * @param compte  le compte ciblé
	 * @param montant la somme a retirer
	 * @return un boolran pour verification
	 */
	public static boolean debiterCompte(CompteBancaires compte, double montant) {
		if (compte != null && montant != 0) {
			compte.setSolde(compte.getSolde() - montant);
			return true;
		}
		return false;
	}

	public static boolean virement(CompteBancaires compteDeb, CompteBancaires compteCred, double montant) {
		if (compteDeb != null && compteCred != null && montant != 0) {
			debiterCompte(compteCred, montant);
			crediterCompte(compteDeb, montant);
			return true;
		}
		return false;
	}
}